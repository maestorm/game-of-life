const playground = document.getElementById('playBoard'); // canvas element
const boarddimension = playground.getContext('2d'); // dimension
playground.style.border = '1.5px solid #777';

let a,
  b,
  axisX,
  axisY,
  roundedX,
  roundedY,
  currentBoard,
  time,
  selectMode,
  generations,
  zeroPoint;

(function () {
  selectMode = false;
  initGrid();
  selectCell();
})();

// selectCell
function selectCell() {
  playground.addEventListener('click', () => {
    if (selectMode == false) {
      clearFields();
      currentBoard = [];
      for (let a = 0; a < 30; a++) {
        currentBoard[a] = [];
        for (let b = 0; b < 30; b++) {
          currentBoard[a][b] = 0;
        }
      }
    }
    let totalOffsetX = 0;
    let totalOffsetY = 0;
    axisX = 0;
    axisY = 0;
    selectMode = true;
    let currentElement = document.getElementById('playBoard');

    do {
      totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
      totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
    } while ((currentElement = currentElement.offsetParent));

    axisX = event.pageX - totalOffsetX;
    axisY = event.pageY - totalOffsetY;
    roundedX = Math.floor(axisX);
    roundedY = Math.floor(axisY);

    while (roundedX % 30 != 0) {
      roundedX--;
    }
    while (roundedY % 30 != 0) {
      roundedY--;
    }

    fillCell(roundedX, roundedY, 1);

    if (roundedX || roundedY != 0) {
      roundedX /= 30;
      roundedY /= 30;
    }
    currentBoard[roundedX][roundedY] = 1; // Here is selected block assigned into currentBoard
  });
}

// random color
function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

// fill cell
function fillCell(xaxis, yaxis, value) {
  const randomColor = getRandomColor();
  if (value == 1) {
    boarddimension.fillStyle = randomColor;
    boarddimension.fillRect(xaxis, yaxis, 29, 29);
  } else if (value == 0) {
    boarddimension.fillStyle = '#fff';
    boarddimension.fillRect(xaxis, yaxis, 29, 29);
  }
}

// init grid
function initGrid() {
  for (let i = 0; i <= 887; i += 30) {
    for (let j = 0; j <= 887; j += 30) {
      boarddimension.lineWidth = 0.5;
      boarddimension.fillStyle = '#777';
      boarddimension.strokeStyle = '#777';
      boarddimension.strokeRect(i, j, 30, 30);
    }
  }
}

// Clear fields
function clearFields() {
  for (let a = 0; a <= 887; a += 30) {
    for (let b = 0; b <= 887; b += 30) {
      fillCell(a, b, 0);
    }
  }
  initGrid();
}

// start game
function gameStart() {
  clearInterval(time);
  if (selectMode == false) {
    currentBoard = [];
    for (a = 0; a <= 90; a++) {
      currentBoard[a] = [];
      for (b = 0; b <= 90; b++) {
        currentBoard[a][b] = Math.round(Math.random());
      }
    }
  }

  time = setInterval(() => {
    generations++,
      gameOfLife(currentBoard),
      (document.getElementById('generationCount').innerHTML = generations);
  }, 100);
}

// controls
document.getElementById('startButton').addEventListener('click', function (e) {
  zeroPoint = false;
  generations = 0;
  gameStart();
});
document.getElementById('overButton').addEventListener('click', function (e) {
  selectMode = false;
  clearFields();
  clearInterval(time);
  document.getElementById('generationCount').innerHTML = 0;
});

// the algorithm
export function countLivingNeighbors(x, y, inputBoard) {
  let count = 0;
  for (let i = x - 1; i <= x + 1; i++) {
    if (i < 0 || i >= inputBoard.length) {
      continue;
    }

    for (let j = y - 1; j <= y + 1; j++) {
      if (j < 0 || j >= inputBoard[i].length) {
        continue;
      }

      if (i === x && j === y) {
        continue;
      }

      if (inputBoard[i][j] === 1) {
        count++;
      }
    }
  }

  return count;
}

export default function gameOfLife(inputBoard) {
  let sum = 0;
  const outputBoard = inputBoard.map((row, x) =>
    row.map((cell, y) => {
      const living = countLivingNeighbors(x, y, inputBoard);
      if (cell === 1) {
        sum += cell;
        if (living < 2) {
          return 0;
        } // Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
        if (living > 3) {
          return 0; // Any live cell with more than three live neighbours dies, as if by overpopulation
        }
      } else if (living === 3) {
        return 1; // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction
      }

      return cell;
    })
  );

  for (a = 0; a < 30; a++) {
    for (b = 0; b < 30; b++) {
      fillCell(a * 30, b * 30, outputBoard[a][b]);
      if (zeroPoint === true && sum === 0) {
        clearInterval(time);
        clearFields();
      }
    }
  }
  zeroPoint = true;
  currentBoard = outputBoard;
  return currentBoard;
}
